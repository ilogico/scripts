#!/bin/sh

apps="$HOME/.local"

# NodeJS
PATH="$PATH:$apps/node/bin"

# .NET
export DOTNET_ROOT="$apps/dotnet-sdk"
PATH="$PATH:$DOTNET_ROOT:$HOME/.dotnet/tools"

# Yarn
PATH="$PATH:$apps/yarn/bin"

# Android
export ANDROID_HOME="$apps/android-sdk"
PATH="$PATH:$ANDROID_HOME/emulator"

# Cargo
PATH="$PATH:$HOME/.cargo/bin"

# Web Assembly Binary Tools
PATH="$PATH:$apps/wabt/bin"
