#!/usr/bin/env bash

usenode() {
    NODE_VERSION=node
    if [ -n "$1" ]; then
        NODE_VERSION="node-$1"
    fi
    PATH=$(echo "$PATH" | sed -E "s#/node(-[[:digit:]]+)?/bin:#/${NODE_VERSION}/bin:#" )
}

for completion in "$HOME/.local/bash.d/"*; do
    . "$completion"
done
